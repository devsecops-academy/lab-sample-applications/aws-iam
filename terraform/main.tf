provider "aws" {
  region     = "eu-central-1"
}

data "local_file" "EC2_Developer_Policy" {
    filename = "dsoa_lab_EC2_Developer_Policy.json"
}

data "local_file" "S3_Support_Policy" {
    filename = "dsoa_lab_S3_Support_Policy.json"
}

data "local_file" "EC2_S3_Admin_Policy" {
    filename = "dsoa_lab_EC2_S3_Admin_Policy.json"
}

resource "aws_iam_group_policy" "EC2_Developer_IAM_Policy" {
  name = "dsoa_lab_EC2_Developer_Policy"
  group = "EC2_Developer_Team"
  policy = data.local_file.EC2_Developer_Policy.content
}

resource "aws_iam_group_policy" "S3_Support_IAM_Policy" {
  name = "dsoa_lab_S3_Support_Policy"
  group = "S3_Support_Team"
  policy = data.local_file.S3_Support_Policy.content
}

resource "aws_iam_group_policy" "EC2_S3_Admin_IAM_Policy" {
  name = "dsoa_lab_EC2_S3_Admin_Policy"
  group = "EC2_S3_Admin_Team"
  policy = data.local_file.EC2_S3_Admin_Policy.content
}
